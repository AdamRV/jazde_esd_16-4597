 package com.viscira.model
{
	import com.viscira.components.SlideShell.view.SlideView;
	import com.viscira.components.SlideShell.vo.SlideDataVO;
	
	
	import flash.events.EventDispatcher;
	
	public class ESD_Model extends EventDispatcher
	{
		private static var _instance:ESD_Model;
		private static var _allowInstantiation:Boolean;

	
		[Embed (source="/assets/thumbs/slide1.png")]private var thumb1:Class;
		[Embed (source="/assets/thumbs/slide2.png")]private var thumb2:Class;
		[Embed (source="/assets/thumbs/slide3.png")]private var thumb3:Class;
		[Embed (source="/assets/thumbs/slide4.png")]private var thumb4:Class;
		[Embed (source="/assets/thumbs/slide5.png")]private var thumb5:Class;
		[Embed (source="/assets/thumbs/slide6.png")]private var thumb6:Class;
		[Embed (source="/assets/thumbs/slide7.png")]private var thumb7:Class;
		[Embed (source="/assets/thumbs/slide8.png")]private var thumb8:Class;
		[Embed (source="/assets/thumbs/slide9.png")]private var thumb9:Class;
		[Embed (source="/assets/thumbs/slide10.png")]private var thumb10:Class;
		[Embed (source="/assets/thumbs/slide11.png")]private var thumb11:Class;
		[Embed (source="/assets/thumbs/slide12.png")]private var thumb12:Class;
		[Embed (source="/assets/thumbs/slide13.png")]private var thumb13:Class;
		[Embed (source="/assets/thumbs/slide14.png")]private var thumb14:Class;
		[Embed (source="/assets/thumbs/slide15.png")]private var thumb15:Class;
		[Embed (source="/assets/thumbs/slide16.png")]private var thumb16:Class;
		[Embed (source="/assets/thumbs/slide17.png")]private var thumb17:Class;
		[Embed (source="/assets/thumbs/slide18.png")]private var thumb18:Class;
		[Embed (source="/assets/thumbs/slide19.png")]private var thumb19:Class;
		[Embed (source="/assets/thumbs/slide20.png")]private var thumb20:Class;
		[Embed (source="/assets/thumbs/slide21.png")]private var thumb21:Class;
		[Embed (source="/assets/thumbs/slide22.png")]private var thumb22:Class;
		[Embed (source="/assets/thumbs/slide23.png")]private var thumb23:Class;
		[Embed (source="/assets/thumbs/slide24.png")]private var thumb24:Class;
		[Embed (source="/assets/thumbs/slide25.png")]private var thumb25:Class;
		[Embed (source="/assets/thumbs/slide26.png")]private var thumb26:Class;
		[Embed (source="/assets/thumbs/slide27.png")]private var thumb27:Class;
		[Embed (source="/assets/thumbs/slide28.png")]private var thumb28:Class;
		[Embed (source="/assets/thumbs/slide29.png")]private var thumb29:Class;
		[Embed (source="/assets/thumbs/slide30.png")]private var thumb30:Class;
		[Embed (source="/assets/thumbs/slide31.png")]private var thumb31:Class;
		[Embed (source="/assets/thumbs/slide32.png")]private var thumb32:Class;
		[Embed (source="/assets/thumbs/slide33.png")]private var thumb33:Class;
		[Embed (source="/assets/thumbs/slide34.png")]private var thumb34:Class;
		[Embed (source="/assets/thumbs/slide35.png")]private var thumb35:Class;
		[Embed (source="/assets/thumbs/slide36.png")]private var thumb36:Class;	
		[Embed (source="/assets/thumbs/slide37.png")]private var thumb37:Class;		
		[Embed (source="/assets/thumbs/slide38.png")]private var thumb38:Class;		
		[Embed (source="/assets/thumbs/slide39.png")]private var thumb39:Class;		
		[Embed (source="/assets/thumbs/slide40.png")]private var thumb40:Class;
		[Embed (source="/assets/thumbs/slide41.png")]private var thumb41:Class;
		[Embed (source="/assets/thumbs/slide42.png")]private var thumb42:Class;
		[Embed (source="/assets/thumbs/slide43.png")]private var thumb43:Class;

		// build manifest here
		private var _slides_manual:Array = 
			[				
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	1, Slide01,     SpeakerNotes01,		"slide1", null, thumb1, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	2, Slide02, 	SpeakerNotes02,		"slide2", null, thumb2, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	3, Slide03, 	SpeakerNotes03,		"slide3", null, thumb3, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	4, Slide04, 	SpeakerNotes04,		"slide4", null, thumb4, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	5, Slide05, 	SpeakerNotes05,		"slide5", null, thumb5, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	6, Slide06, 	SpeakerNotes06,		"slide6", null, thumb6, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	7, Slide07, 	SpeakerNotes07,		"slide7", null, thumb7, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	8, Slide08, 	SpeakerNotes08,		"slide8", null, thumb8, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	9, Slide09, 	SpeakerNotes09,		"slide9", null, thumb9, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	10, Slide10, 	SpeakerNotes10,		"slide10", null, thumb10, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	11, Slide11, 	SpeakerNotes11,		"slide11", null, thumb11, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	12, Slide12, 	SpeakerNotes12,		"slide12", null, thumb12, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	13, Slide13, 	SpeakerNotes13,		"slide13", null, thumb13, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	14, Slide14, 	SpeakerNotes14,		"slide14", null, thumb14, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	15, Slide15, 	SpeakerNotes15,		"slide15", null, thumb15, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	16, Slide16, 	SpeakerNotes16,		"slide16", null, thumb16, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	17, Slide17, 	SpeakerNotes17,		"slide17", null, thumb17, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	18, Slide18, 	SpeakerNotes18,		"slide18", null, thumb18, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	19, Slide19, 	SpeakerNotes19,		"slide19", null, thumb19, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	20, Slide20, 	SpeakerNotes20,		"slide20", null, thumb20, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	21, Slide21, 	SpeakerNotes21,		"slide21", null, thumb21, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS,  22, Slide22, 	SpeakerNotes22,		"slide22", null, thumb22, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	23, Slide23, 	SpeakerNotes23,		"slide23", null, thumb23, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	24, Slide24, 	SpeakerNotes24,		"slide24", null, thumb24, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	25, Slide25, 	SpeakerNotes25,		"slide25", null, thumb25, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	26, Slide26, 	SpeakerNotes26,		"slide26", null, thumb26, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	27, Slide27, 	SpeakerNotes27,		"slide27", null, thumb27, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	28, Slide28, 	SpeakerNotes28,		"slide28", null, thumb28, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	29, Slide29, 	SpeakerNotes29,		"slide29", null, thumb29, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	30, Slide30, 	SpeakerNotes30,		"slide30", null, thumb30, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	31, Slide31, 	SpeakerNotes31,		"slide31", null, thumb31, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	32, Slide32, 	SpeakerNotes32,		"slide32", null, thumb32, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	33, Slide33, 	SpeakerNotes33,		"slide33", null, thumb33, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	34, Slide34, 	SpeakerNotes34,		"slide34", null, thumb34, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	35, Slide35, 	SpeakerNotes35,		"slide35", null, thumb35, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	36, Slide36, 	SpeakerNotes36,		"slide35", null, thumb36, "false"),	
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	37, Slide37, 	SpeakerNotes37,		"slide36", null, thumb37, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	38, Slide38, 	SpeakerNotes38,		"slide37", null, thumb38, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	39, Slide39, 	SpeakerNotes39,		"slide38", null, thumb39, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	40, Slide40, 	SpeakerNotes40,		"slide39", null, thumb40, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	41, Slide41, 	SpeakerNotes41,		"slide40", null, thumb41, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	42, Slide42, 	SpeakerNotes42,		"slide41", null, thumb42, "false"),
				new SlideDataVO(SlideView.GREENSOCK_CLASS, 	43, Slide43, 	SpeakerNotes43,		"slide42", null, thumb43, "false")
			];
		
		public function ESD_Model()
		{
			super();
			if (!_allowInstantiation) {
				throw new Error("Error: Instantiation failed: Use ESD_Model.instance instead of new.");
			}
		}
		
		public static function get instance():ESD_Model {
			if (_instance == null) {
				_allowInstantiation = true;
				_instance = new ESD_Model();
				_allowInstantiation = false;
			}
			return _instance;
		}
		
		public function get slides():Array{
			return _slides_manual;
		}
		
		private var _mode:String;
		public function set mode(val:String):void{
			_mode = val;
		}
		
		public function get mode():String{
			return _mode;
		}
	}
}